import React from 'react'
import ReactDOM from 'react-dom'

function ZodiacTable () {
	return(
		<div>
			<thead>Знайди свій знак</thead>
			<table>
			<tr class='top'>
				<td>Знак зодіаку</td>
				<td>Тривалість: з - по</td>
				<td>Знак зодіак</td>
				<td>Тривалість: з - по</td>
			</tr>
			<tr>
				<td>Овен</td>
				<td>21.03 - 20.04</td>
				<td>Телець</td>
				<td>21.04 - 21.05</td>
			</tr>
			<tr>
				<td>Близнюки</td>
				<td>22.05 - 21.06</td>
				<td>Рак</td>
				<td>22.06 - 22.07</td>
			</tr>
			<tr>
				<td>Лев</td>
				<td>23.07 - 21.08</td>
				<td>Діва</td>
				<td>22.08 - 23.09</td>
			</tr>
			<tr>
				<td>Терези</td>
				<td>23.09 - 22.10</td>
				<td>Скорпіон</td>
				<td>23.10 - 21.11</td>
			</tr>
			<tr>
				<td>Стрілець</td>
				<td>22.11 - 21.12</td>
				<td>Козеріг</td>
				<td>22.12 - 19.01</td>
			</tr>
			<tr>
				<td>Водолій</td>
				<td>20.01 - 18.02</td>
				<td>Риби</td>
				<td>19.02 - 20.03</td>
			</tr>
			</table>
		</div>
	)
}

ReactDOM.render(<ZodiacTable></ZodiacTable>, document.querySelector('.root'))